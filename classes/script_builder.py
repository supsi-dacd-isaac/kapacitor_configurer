import json

class ScriptsBuilder:
    """
    Class to build Kapacitor script
    """
    def __init__(self, cfg, logger):
        """
        Constructor
        :param cfg: configuration dictionary
        :type dict
        :param logger
        :type Logger
        """
        self.cfg = cfg
        self.logger = logger

    def create_tick(self):
        fw = open('%s/%s.tick' % (self.cfg['tick']['folder'], self.cfg['id']), 'w')

        # header section
        fw.write('dbrp %s\n' % self.cfg['tick']['pars']['dbrp'])
        fw.write('\n')
        fw.write('var data = batch\n')

        # query section
        fw.write('\t|query(\'\'\'\n')
        fw.write('\t\t%s\n' % self.cfg['tick']['pars']['query']['select'])
        fw.write('\t\t%s\n' % self.cfg['tick']['pars']['query']['from'])
        fw.write('\t\t%s\n' % self.cfg['tick']['pars']['query']['where'])
        fw.write('\t\'\'\')\n')
        fw.write('\t\t.period(%s)\n' % self.cfg['tick']['pars']['period'])
        fw.write('\t\t.every(%s)\n' % self.cfg['tick']['pars']['every'])
        str_grp = '.groupBy('
        for grp in self.cfg['tick']['pars']['grouping']:
            str_grp += '%s, ' % grp
        str_grp = '%s)' % str_grp[0:-2]
        fw.write('\t\t%s\n' % str_grp)

        # alert section
        if self.cfg['tick']['pars']['deadman'] is False:
            fw.write('data\n')
            fw.write('\t|alert()\n')
            if 'warn' in self.cfg['tick']['pars'].keys():
                fw.write('\t\t.warn(lambda: %s)\n' % self.cfg['tick']['pars']['warn'])
            if 'crit' in self.cfg['tick']['pars'].keys():
                fw.write('\t\t.crit(lambda: %s)\n' % self.cfg['tick']['pars']['crit'])

        else:
            fw.write('data\n')
            fw.write('\t|deadman(0.0, %s)\n' % self.cfg['tick']['pars']['every'])

        # output section
        for output in self.cfg['tick']['pars']['outputs']:
            # Aggregator topic
            if output['case'] == 'aggregator_topic':
                fw.write('\t\t.topic(\'%s_aggregate\')\n' % self.cfg['id'])

            # Topic
            elif output['case'] == 'topic':
                fw.write('\t\t.topic(\'%s\')\n' % output['name'])

            # Slack
            elif output['case'] == 'slack':
                fw.write('\t\t.slack()\n')

            # Log file
            elif output['case'] == 'log':
                fw.write('\t\t.log(\'%s/%s.log\')\n' % (output['folder'], self.cfg['id']))
        fw.close()

    def create_yaml_aggregator(self):

        fw = open('%s/%s.yaml' % (self.cfg['yaml']['folder'], self.cfg['id']), 'w')
        fw.write('id: %s_aggregate\n' % self.cfg['id'])
        fw.write('topic: %s_aggregate\n' % self.cfg['id'])
        fw.write('kind: aggregate\n')
        fw.write('options:\n')
        fw.write('  interval: %i\n' % int(float(self.cfg['yaml']['every']) * 1e9))
        fw.write('  topic: %s\n' % self.cfg['yaml']['senderTopic'])
        fw.write('  message: %s -> %s\n' % (self.cfg['id'], self.cfg['yaml']['message']))
        fw.close()
