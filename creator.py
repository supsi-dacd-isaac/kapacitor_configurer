# --------------------------------------------------------------------------- #
# Import section
# --------------------------------------------------------------------------- #
import argparse
import json
import logging
import glob

from classes.script_builder import ScriptsBuilder

# --------------------------------------------------------------------------- #
# Main
# --------------------------------------------------------------------------- #
if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-f', help='input folder', required=True)

    # Set configuration
    args = arg_parser.parse_args()

    logger = logging.getLogger()
    logging.basicConfig(format='%(asctime)-15s::%(levelname)s::%(funcName)s::%(message)s', level=logging.INFO)

    logger.info('Start the program')

    for config_file in glob.glob('%s/*.json' % args.f):
        logger.info('Get data from file %s' % config_file)
        config = json.loads(open(config_file).read())

        sb = ScriptsBuilder(cfg=config, logger=logger)

        logger.info('Created TICK script in %s%s.tick' % (config['tick']['folder'], config['id']))
        sb.create_tick()

        logger.info('Created YAML script in %s%s.yaml' % (config['yaml']['folder'], config['id']))
        sb.create_yaml_aggregator()

    logger.info('Scripts created')
