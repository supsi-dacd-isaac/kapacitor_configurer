#!/bin/bash

log() {
  echo $(date +"%Y-%m-%d %H:%M:%S")' - '$1
}

while getopts f: option
do
  case "${option}" in
      f) CFG_FOLDER=${OPTARG};;
esac
done

# Cycle over the JSON files in the folder
for CFG_FILE in $CFG_FOLDER/*.json
do
    # Get variables from JSON file
    TASK_NAME=$(cat $CFG_FILE | jq -r '.id')
    TICK_SCRIPT=$(cat $CFG_FILE | jq -r '.tick.folder')/$TASK_NAME.tick
    YAML_AGGREGATOR_SCRIPT=$(cat $CFG_FILE | jq -r '.yaml.folder')/$TASK_NAME.yaml

    log "Create and enable the task "$TASK_NAME
    kapacitor define $TASK_NAME -tick $TICK_SCRIPT
    kapacitor enable $TASK_NAME

    log "Create and enable the aggregator topic "$YAML_AGGREGATOR_SCRIPT
    kapacitor define-topic-handler $YAML_AGGREGATOR_SCRIPT

    log "Create the svg file"
    kapacitor show $TASK_NAME > $TICK_SCRIPT.tmp1
    sed -n -e '/DOT/,$p' $TICK_SCRIPT.tmp1 > $TICK_SCRIPT.tmp2
    tail -n +2 $TICK_SCRIPT.tmp2 > $TICK_SCRIPT.dot
    PARENT_FOLDER=$(dirname "${TICK_SCRIPT}")
    dot -Tsvg $TICK_SCRIPT.dot > $PARENT_FOLDER/svg/$TASK_NAME.svg
    rm $TICK_SCRIPT.tmp1
    rm $TICK_SCRIPT.tmp2
    rm $TICK_SCRIPT.dot

    log "Show task list"
    kapacitor list tasks
    log "Show topics list"
    kapacitor list topics
done